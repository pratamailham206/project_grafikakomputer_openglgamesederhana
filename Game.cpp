#include "Game.h"

Game::Game() {

	player1.setPosition(5.0, 10.0, 5.0, 10.0);
	player2.setPosition(5.0, 10.0, 40.0, 45.0);

	finishLine.setSize(110.0, 90.0, 100.0, 0.0);

	collision.setSize(25, 20, 40, 10);
	collision2.setSize(35, 30, 60, 20);

	collision3.setSize(75, 70, 80, 10);
	collision4.setSize(85, 80, 40, 20);
	collision5.setSize(55, 50, 80, 20);

	objCollision.setSizeLight(30.0, 45.0, 30.0, 45.0);

	cout << "Start" << endl;
}

void Game::printText(const char *text, int length, int x, int y) {
	glMatrixMode(GL_PROJECTION);
	double* matrix = new double[16];
	glGetDoublev(GL_PROJECTION_MATRIX, matrix);
	glLoadIdentity();
	glOrtho(0, 800, 0, 600, -5, 5);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	glLoadIdentity();
	glRasterPos2i(x, y);
	for (int i = 0; i < length; i++) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, (int)text[i]);
	}
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(matrix);
	glMatrixMode(GL_MODELVIEW);
}

void Game::Restart() {
	if (!playing) {
		playing = true;
		glutPostRedisplay();
	}
}

void Game::Start() {
	glClear(GL_COLOR_BUFFER_BIT);
	glPushMatrix();	

	if (playing) {
		finishLine.setColor(0.0, 0.0, 1.0);

		finishLine.draw();

		collision.draw();
		collision.moveObject();

		collision2.draw();
		collision2.moveObject();

		collision3.draw();
		collision3.moveObject();

		collision4.draw();
		collision4.moveObject();

		collision5.draw();
		collision5.moveObject();

		player1.setColor(0.0, 1.0, 0.0);
		player1.draw();

		player2.setColor(1.0, 0.0, 0.0);
		player2.draw();

		status = finishLine.detectCol(player1, player2);

		if (collision.DetectCol(player1, player2) == 1) {
			player1.RestartPos(5.0, 10.0, 5.0, 10.0);
		}

		if (collision.DetectCol(player1, player2) == 2) {
			player2.RestartPos(5.0, 10.0, 40.0, 45.0);
		}

		if (collision2.DetectCol(player1, player2) == 1) {
			player1.RestartPos(5.0, 10.0, 5.0, 10.0);
		}

		if (collision2.DetectCol(player1, player2) == 2) {
			player2.RestartPos(5.0, 10.0, 40.0, 45.0);
		}

		if (collision3.DetectCol(player1, player2) == 1) {
			player1.RestartPos(5.0, 10.0, 5.0, 10.0);
		}

		if (collision3.DetectCol(player1, player2) == 2) {
			player2.RestartPos(5.0, 10.0, 40.0, 45.0);
		}

		if (collision4.DetectCol(player1, player2) == 1) {
			player1.RestartPos(5.0, 10.0, 5.0, 10.0);
		}

		if (collision4.DetectCol(player1, player2) == 2) {
			player2.RestartPos(5.0, 10.0, 40.0, 45.0);
		}

		if (collision5.DetectCol(player1, player2) == 1) {
			player1.RestartPos(5.0, 10.0, 5.0, 10.0);
		}

		if (collision5.DetectCol(player1, player2) == 2) {
			player2.RestartPos(5.0, 10.0, 40.0, 45.0);
		}

		if (status == 1) {
			player1.RestartPos(5.0, 10.0, 5.0, 10.0);
			playing = false;
		}
		if (status == 2) {
			player2.RestartPos(5.0, 10.0, 40.0, 45.0);
			playing = false;
		}

		string text = "Player 1 Score : " + to_string(player1.getScore());		
		glColor3f(0, 0, 0);		
		printText(text.data(), text.size(), 35, 570);

		string text1 = "Player 2 Score : " + to_string(player2.getScore());
		glColor3f(0, 0, 0);
		printText(text1.data(), text1.size(), 35, 540);
	}

	else if (!playing) {
		if (status == 1) {			
			string text = "Player 1 Win";
			glColor3f(0, 0, 0);
			printText(text.data(), text.size(), 135, 500);
			string text2 = "Press R to restart game";
			printText(text2.data(), text2.size(), 135, 460);
			player1.increaseScore();
		}
		else {
			string text = "Player 2 Win";
			glColor3f(0, 0, 0);
			printText(text.data(), text.size(), 135, 500);
			string text2 = "Press R to restart game";
			printText(text2.data(), text2.size(), 135, 460);
			player2.increaseScore();
		}		
	}	

	glPopMatrix();
	glutSwapBuffers();
	glFlush();
}