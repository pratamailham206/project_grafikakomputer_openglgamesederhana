#pragma once
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>  
#include<string>  

#include "Player.h"
#include "FinishLine.h"
#include "MovingCollision.h"
#include "collisionLight.h"

using namespace std;

class Game
{
private:
    float scaleValue = 1;
    float translateValueX = 0;
    float translateValueY = 0;

    FinishLine finishLine;
    MovingCollision collision;
    MovingCollision collision2;
    MovingCollision collision3;
    MovingCollision collision4;
    MovingCollision collision5;
    collisionLight objCollision;

    float player1maxX;
    float player1minX;
    float player1maxY;
    float player1minY;

    int status;
    bool playing = true;

public:
    Player player1;
    Player player2;

    Game();
    void Start();
    void printText(const char *text, int length, int x, int y);
    void Restart();
};

