#pragma once
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>
using namespace std;

class collisionLight
{
private:
	float minX;
	float maxX;
	float minY;
	float maxY;

public:
	void setSizeLight(float, float, float, float);
	void draw();
	static void update(int value)
	{
		float angle{};

		angle += 30.0;

		if (angle == 360.0) {
			angle -= 360;
		}

		glutPostRedisplay();
		glutTimerFunc(10.0, update, 5.0);
	};
};

