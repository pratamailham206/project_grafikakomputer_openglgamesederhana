#pragma once
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>  

#include "Player.h";

using namespace std;

class MovingCollision
{
private:
	float minX;
	float maxX;
	float minY;
	float maxY;
	bool up = true;

public:
	void setSize(float, float, float, float);
	void draw();
	void moveObject();
	int DetectCol(Player, Player);
};

