#pragma once
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>  

using namespace std;

class Player
{
private:
	float minX;
	float minY;
	float maxX;
	float maxY;

	int score = 0;

public:
	void draw();
	void setPosition(float, float, float, float);
	void setColor(float, float, float);

	void moveLeft();
	void moveRight();
	void moveUp();
	void moveDown();

	float getXmaxPos();
	float getXminPos();
	float getYmaxPos();
	float getYminPos();

	void RestartPos(float, float, float, float);

	void increaseScore();
	int getScore();
};

