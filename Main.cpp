#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>  
#include "Game.h"

using namespace std;
#define PI 3.14159265

Game game;
// End globals.

// Drawing routine.
void drawScene(void)
{
	game.Start();
}

// Initialization routine.
void setup(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 100.0, 0.0, 100.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'a':
		game.player1.moveLeft();
		break;
	case 'd':
		game.player1.moveRight();
		break;
	case 'w':
		game.player1.moveUp();
		break;
	case 's':
		game.player1.moveDown();
		break;
	case 'j':
		game.player2.moveLeft();
		break;
	case 'l':
		game.player2.moveRight();
		break;
	case 'i':
		game.player2.moveUp();
		break;
	case 'k':
		game.player2.moveDown();
		break;
	case 'r':
		game.Restart();
		break;
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
	std::cout << "Interaction:" << std::endl;
	std::cout << "Press the space bar to toggle between wireframe and filled." << std::endl;
}

// Main routine.
int main(int argc, char** argv)
{
	printInteraction();
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(920, 920);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Race Runners");
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);

	glewExperimental = GL_TRUE;
	glewInit();

	setup();

	glutMainLoop();
}
