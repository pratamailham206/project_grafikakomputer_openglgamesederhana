## About

A racing game with many moving obstacles in the arena. play with 2 players to see who is faster. 

Created with openGL

## Genre

Racing

## Platform

Microsof Windows

## Gameplay

Play with 2 players, avoid all of the obstacles, and cross the finish line, adjust the timing to avoid the obstacles.
Xross the finish line early and be the winner.

## Development Team
- Ilham Pratama
- Dicky Dwi Darmawan


## Screenshot

![](images/s1.PNG)